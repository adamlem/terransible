FROM lexauw/ansible-alpine:v2.8.7

ENV TERRAFORM_VERSION=0.12.23
ENV KUBE_LATEST_VERSION="v1.17.4"
ENV HELM_VERSION="v2.16.3"
ENV AWSCLI_VERSION="1.18.25"
ENV AWSIAMAUTHENTICATOR_VERSION="1.15.10/2020-02-22"
ENV OPENSHIFT_VERSION="0.10.1"
ENV BOTO3_VERSION="1.12.25"
ENV BOTOCORE_VERSION="1.15.25"
ENV URLLIB3_VERSION="1.25.8"
ENV S3TRANSFER_VERSION="0.3.3"
ENV HELM_SHELL_VERSION="master"

RUN apk add --no-cache --update git curl build-base python3-dev && mkdir -p ~/.ansible/plugins/modules && \
  echo "download custom helm_shell module" && \
  curl -L https://raw.githubusercontent.com/xmfreak/helm-shell-ansible-module/${HELM_SHELL_VERSION}/helm_shell.py -o ~/.ansible/plugins/modules/helm_shell.py && \
  echo "install awscli, openshift etc python dependencies" && \
  pip install awscli==${AWSCLI_VERSION} openshift==${OPENSHIFT_VERSION} boto3==${BOTO3_VERSION} botocore==${BOTOCORE_VERSION} urllib3==${URLLIB3_VERSION} s3transfer==${S3TRANSFER_VERSION} && \
  echo "download aws-iam-authenticator" && \
  curl -L https://amazon-eks.s3-us-west-2.amazonaws.com/${AWSIAMAUTHENTICATOR_VERSION}/bin/linux/amd64/aws-iam-authenticator -o /usr/local/bin/aws-iam-authenticator && \
  echo "download kubectl" && \
  curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl && \
  echo "download terraform" && \
  curl -L https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
  unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin && \
  rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
  echo "download helm" && \
  wget -q https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm && \
  chmod +x /usr/local/bin/* && \
  echo "install NodeJs" && \
  apk update && apk upgrade && \
  apk add nodejs && apk add npm && \
  echo "cleanup build dependencies" && \
  apk del build-base python3-dev

ENTRYPOINT []
